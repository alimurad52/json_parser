package com.example.alimurad.json_parser;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

/**
 * Created by alimurad on 19/12/2016.
 */

public class Skills extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        ScrollView scrollView = new ScrollView(this);
        scrollView.addView(linearLayout);
        this.setContentView(scrollView);

        try{
            JSONObject jsonObject = new JSONObject(jsonLoader());

            JSONArray workArray =  jsonObject.getJSONArray("skills");

            for(int i =0; i <workArray.length(); i++) {
                JSONObject jtemp =  workArray.getJSONObject(i);
                Iterator<String> iter = jtemp.keys();
                while(iter.hasNext()) {
                    try{
                        String tag = iter.next();
                        TextView textView = new TextView(this);
                        textView.setText(tag);
                        textView.setTypeface(null, Typeface.BOLD);
                        textView.setTextSize(20);
                        linearLayout.addView(textView);

                        String value = jtemp.getString(tag);
                        TextView textView1 = new TextView(this);
                        textView1.setText(value);
                        textView1.setTextSize(16);
                        linearLayout.addView(textView1);
                    } catch (JSONException e) {
                        e.printStackTrace();;
                    }
                }
                View view = new View(this);
                view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 5));
                view.setBackgroundColor(Color.BLACK);
                linearLayout.addView(view);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private String jsonLoader() {
        String json = null;
        try {
            InputStream inputStream = getAssets().open("resume.json");
            int size = inputStream.available();
            byte[] buff = new byte[size];
            inputStream.read(buff);
            inputStream.close();
            json = new String(buff, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }
}
