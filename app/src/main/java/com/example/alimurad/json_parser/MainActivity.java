package com.example.alimurad.json_parser;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    Button bt_work, bt_skill, bt_basics, bt_awards, bt_educations, bt_languages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt_work = (Button) findViewById(R.id.bt_work);
        bt_basics = (Button) findViewById(R.id.bt_basics);
        bt_educations = (Button) findViewById(R.id.bt_education);
        bt_awards = (Button) findViewById(R.id.bt_awards);
        bt_skill = (Button) findViewById(R.id.bt_sills);
        bt_languages = (Button) findViewById(R.id.bt_languages);
        bt_work.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent work = new Intent(getBaseContext(), Work.class);
                startActivity(work);
            }
        });
        bt_basics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent basics = new Intent(getBaseContext(), Basics.class);
                startActivity(basics);
            }
        });
        bt_educations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent education = new Intent(getBaseContext(), Education.class);
                startActivity(education);
            }
        });
        bt_awards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent awards = new Intent(getBaseContext(), Awards.class);
                startActivity(awards);
            }
        });
        bt_skill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent skills = new Intent(getBaseContext(), Skills.class);
                startActivity(skills);
            }
        });
        bt_languages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent languages = new Intent(getBaseContext(), Languages.class);
                startActivity(languages);
            }
        });
    }
}
